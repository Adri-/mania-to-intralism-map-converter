"use strict";

const prompt = require('prompt-sync')();
const walkSync = require('walk-sync');
var TrieSearch = require('trie-search');
var ffmpeg = require('easy-ffmpeg');
var each = require('foreach');
const fs = require('fs');
process.stdin.setEncoding('utf8');
var clipper = require('image-clipper');
var shell = require('shelljs');

var ts = new TrieSearch(['Title', 'Artist', 'Version', 'Creator']);
var date = new Date();

var config;
var cache;
function saveConfig() {
    fs.writeFileSync("config.json", JSON.stringify(config), { encoding: 'utf8', flag: 'w' });
}
function saveCache() {
    fs.writeFileSync("cache.json", JSON.stringify(cache), { encoding: 'utf8', flag: 'w' });
}

function checkConfig() {
    try {
        config = require("./config.json");
    } catch {
        config = {
            "intralism": {
                "folder": null,
                "workshop": null,
                "options": {
                    "alwaysUseCache": true,
                    "skipCacheDiff": false,
                    "alwaysAskCacheReset": false
                }
            },
            "osu": {
                "folder": null,
                "options": {
                    "alwaysUseCache": true,
                    "skipCacheDiff": false,
                    "alwaysAskCacheReset": false
                }
            }
        }
        saveConfig();
    }

    try {
        cache = require("./cache.json");
    } catch {
        cache = {
            "osu": {
                "songs": null,
                "updated": null,
            },
            "intralism": {
                "songs": null,
                "updated": null,
            },
        };
    }
    saveCache();
}

function getOsuFolder() {
    var folder = config.osu.folder;

    if (folder == null) {
        var found = false;
        while (!found) {
            folder = prompt("Please enter the path to your osu! folder, or exit to abort : ");
            if (folder == "exit") {
                process.exit();
            }

            try {
                var f = fs.statSync(folder);
                if (f.isDirectory()) {
                    var s = fs.statSync(folder + "/Songs");
                    if (s.isDirectory()) {
                        found = true;
                    } else {
                        console.log("This directory does not contain a 'Songs' folder.\n");
                    }
                } else {
                    console.log("This directory does not exist.\n");
                }
            } catch (Exception) {
                console.log("Invalid directory.\n");
            }
        }

        if (prompt("Remember this path ? (y,n) ").toLowerCase() == "y") {
            config.osu.folder = folder;
            saveConfig();
        }
    }

    return folder;
}

function getWorkshopFolder() {
    var folder = config.intralism.workshop;

    if (folder == null) {
        var found = false;
        while (!found) {
            folder = prompt("Please enter the path to your Intralism Workshop folder (steamapps\\workshop\\content\\513510), or exit to abort : ");
            if (folder == "exit") {
                process.exit();
            }

            try {
                var f = fs.statSync(folder);
                if (f.isDirectory()) {
                    found = true;
                } else {
                    console.log("This directory does not exist.\n");
                }
            } catch (Exception) {
                console.log("Invalid directory.\n");
            }
        }

        if (prompt("Remember this path ? (y,n) ").toLowerCase() == "y") {
            config.intralism.workshop = folder;
            saveConfig();
        }
    }

    return folder;
}

function getIntralismFolder() {
    var folder = config.intralism.folder;

    if (folder == null) {
        var found = false;
        while (!found) {
            folder = prompt("Please enter the path to your Intralism folder (steamapps\\common\\Intralism), or exit to abort : ");
            if (folder == "exit") {
                process.exit();
            }

            try {
                var f = fs.statSync(folder);
                if (f.isDirectory()) {
                    var s = fs.statSync(folder + "/Editor");
                    if (s.isDirectory()) {
                        found = true;
                    } else {
                        console.log("This directory does not contain a 'Editor' folder.\n");
                    }
                } else {
                    console.log("This directory does not exist.\n");
                }
            } catch (Exception) {
                console.log("Invalid directory.\n");
            }
        }

        if (prompt("Remember this path ? (y,n) ").toLowerCase() == "y") {
            config.intralism.folder = folder;
            saveConfig();
        }
    }

    return folder;
}

var metadata;
function scanFolderBeatmaps(path, name) {
    var folder = path + "/" + name;
    var folderContents = walkSync(folder);

    var mapset = {};
    var diffs = 0;

    each(folderContents, function (val, k, o) {
        var p = folder + "/" + val;
        if (val.slice(-4) == ".osu") {
            if (metadata[name] == null || metadata[name][val] == null) {
                var map = {
                    path: p,
                };

                var file = fs.readFileSync(map.path, { encoding: 'utf8' });
                var invalidated = false;

                each(file.split(/\r?\n|\r/g), function (line, n, obj) {
                    var s = line.split(":");
                    if (s.length == 2) {
                        var key = s[0];
                        switch (key) {
                            case "Mode":
                                if (s[1] != "3" && s[1] != " 3") {
                                    invalidated = true;
                                }
                                break;
                            case "Title":
                            case "Artist":
                            case "AudioFilename":
                            case "Version":
                            case "Creator":
                                s.shift();
                                map[key] = s.join(":").trim();
                                break;
                            default:
                        }
                    }
                });
                if (!invalidated) {
                    if (mapset[val] == null) {
                        mapset[val] = {};
                    }
                    mapset[val] = map;
                    diffs++;
                }
            } else {
                mapset = metadata[name]
            }
        }
    });
    if (diffs > 0) {
        metadata[name] = mapset;
    }
}

function scanFolderIntralismBeatmap(path, name) {
    var folder = path + "/" + name;
    var folderContents = walkSync(folder);

    each(folderContents, function (val, k, o) {
        var p = folder + "/" + val;
        if (val == "config.txt") {
            var contents = fs.readFileSync(folder + "/" + val, { encoding: 'utf8' }).trim();
            try {
                var info = JSON.parse(contents);

                var version = "Unknown";
                var versionCheck = info.info.split("[");
                if (versionCheck.length > 1) {
                    version = versionCheck[1].split("]")[0].trim();
                }

                var artist = "Unknown";
                var artistCheck = info.name.split("-");

                if (artistCheck.length > 1) {
                    artist = artistCheck[0].trim();
                } else {
                    // Because stupid characters
                    artistCheck = info.name.split("–");
                    if (artistCheck.length > 1) {
                        artist = artistCheck[0].trim();
                    }
                }

                var title = info.name;
                var titleCheck = info.name.split("-");
                if (titleCheck.length > 1) {
                    title = titleCheck[1].trim();
                } else {
                    // Because stupid characters
                    titleCheck = info.name.split("–");
                    if (titleCheck.length > 1) {
                        title = titleCheck[0].trim();
                    }
                }

                var map = {
                    path: p,
                    Title: title,
                    Artist: artist,
                    AudioFilename: info.musicFile,
                    Version: version,
                    Creator: "Unknown", // No info
                };

                metadata[info.name + info.info] = [map];
            } catch {
                console.log(folder + " is malformed. Delet plz.");
            }
        }
    });

}

function scanPhase(path, songs) {
    var scan = true;
    var cached = true;

    var cacheDate = new Date(cache.osu.updated).toLocaleString();

    if (metadata == null) {
        metadata = {};
        console.log("A full first scan of your Songs directory will be peformed, this can take a while.");
    } else {
        var reset = false;
        if (!config.osu.options.alwaysUseCache) {
            if (config.osu.options.alwaysAskCacheReset) {
                var resetCachePrompt = prompt("A previous scan was cached (" + cacheDate + "), use it ? (y,n) ");

                if (resetCachePrompt && resetCachePrompt.toLowerCase() == 'n') {
                    metadata = {};
                    cache.osu.updated = null;
                    reset = true;
                }
            }
        }

        if (!reset && !config.osu.options.skipCacheDiff) {
            var useCachePrompt = prompt("Do you want to only use the latest cache (" + cacheDate + ") and skip the scandiff (should be short) ? (y,n) ");

            if (useCachePrompt && useCachePrompt.toLowerCase() == 'y') {
                scan = false;
                cached = false;
            }
        } else if (config.osu.options.skipCacheDiff) {
            scan = false;
            cached = false;
        }
    }

    if (scan) {
        var step = 100;
        var i = 0;

        console.log("Loading mapsets in " + songs.length + " directories.");
        each(songs, function (val, k, o) {
            i++
            if (i % step == 0) {
                console.log(Math.round(i * 100 / songs.length) + "%");
            }
            scanFolderBeatmaps(path, val);
        });
    }

    if (cached) {
        cache.osu.songs = metadata;
        cache.osu.updated = date.getTime();
        saveCache();
        saveConfig();
    }
}

function intralismScanPhase(path, songs) {
    var scan = true;
    var cached = true;

    var cacheDate = new Date(cache.intralism.updated).toLocaleString();

    if (metadata == null) {
        metadata = {};
        console.log("A full first scan of your Intralism Workshop directory will be peformed, this can take a while.");
    } else {
        var reset = false;
        if (!config.intralism.options.alwaysUseCache) {
            if (config.intralism.options.alwaysAskCacheReset) {
                var resetCachePrompt = prompt("A previous scan was cached (" + cacheDate + "), use it ? (y,n) ");

                if (resetCachePrompt && resetCachePrompt.toLowerCase() == 'n') {
                    metadata = {};
                    cache.intralism.updated = null;
                    reset = true;
                }
            }
        }

        if (!reset && !config.intralism.options.skipCacheDiff) {
            var useCachePrompt = prompt("Do you want to only use the latest cache (" + cacheDate + ") and skip the scandiff (should be short) ? (y,n) ");

            if (useCachePrompt && useCachePrompt.toLowerCase() == 'y') {
                scan = false;
                cached = false;
            }
        } else if (config.intralism.options.skipCacheDiff) {
            scan = false;
            cached = false;
        }
    }

    if (scan) {
        var step = 1000;
        var i = 0;

        console.log("Loading intralism mapsets in " + songs.length + " directories.");
        each(songs, function (val, k, o) {
            i++
            if (i % step == 0) {
                console.log(Math.round(i * 100 / songs.length) + "%");
            }
            scanFolderIntralismBeatmap(path, val);
        });
    }

    if (cached) {
        cache.intralism.songs = metadata;
        cache.intralism.updated = date.getTime();
        saveCache();
        saveConfig();
    }
}

function selectBeatmap() {
    var searchable = [];
    each(metadata, function (v, k, o) {
        each(v, function (map, kk, oo) {
            searchable.push(map);
        });
    });
    ts.addAll(searchable);

    var found = false;
    var search = prompt("Search anything : ");
    var selected = 0;

    while (!found) {
        var results = [];
        var i = 1;
        each(ts.get(search), function (v, k, o) {
            var id = v.Artist + " - " + v.Title + " [" + v.Version + "] (" + v.Creator + ")";
            console.log("(" + i + ") - " + id);
            results.push(v);
            i++;
        });

        var valid = false;
        while (!valid) {
            var response = prompt("Enter the desired ID to convert, or search for another map : ");

            if (response == "exit") {
                process.exit();
            }

            if (response != null) {
                valid = true;

                if (isNaN(response)) {
                    search = response;
                } else {
                    selected = parseInt(response) - 1;
                    if (selected >= 0 && selected < i) {
                        found = true;
                    } else {
                        valid = false;
                    }
                }
            }
        }
    }

    return { "results": results, "selected": results[selected] };
}

function parseBeatmap(map) {
    var beatmap = {
        Version: map.Version.replace(/[\"\\\/]/g, ''),
        Artist: map.Artist.replace(/[\"\\\/]/g, ''),
        Title: map.Title.replace(/[\"\\\/]/g, ''),
        Creator: map.Creator.replace(/[\"\\\/]/g, ''),
        Background: null,
        HitObjects: [],
        timingPoints: [],
    };

    var file = fs.readFileSync(map.path, { encoding: 'utf8' });
    var objects = false;
    var timingpoints = false;
    var assets = false;

    each(file.split(/\r?\n|\r/g), function (line, n, obj) {
        if (objects) {
            var hitObject = {
                x: 0,
                time: 0,
            }
            var parts = line.split(",");
            if (parts.length > 2) {

                hitObject.x = parseInt(parts[0]);
                hitObject.time = parseInt(parts[2]);

                beatmap.HitObjects.push(hitObject);
            }
        } else if (timingpoints) {
            var timingPoint = {
                val: 0,
                time: 0,
            }
            var parts = line.split(",");
            if (parts.length >= 2) {

                timingPoint.val = parseInt(parts[1]);
                timingPoint.time = parseInt(parts[0]);

                beatmap.timingPoints.push(timingPoint);
            }
        } 
        else if (assets) {
            var parts = line.split('"');
            var bg = null;
            if (parts.length > 2) {
                bg = parts[1];
            }
            if (bg != null) {
                beatmap.Background = bg;
            }
            assets = false;
        } 
        
        if (line == "//Background and Video events") {
            assets = true;
        } else if (line == "[HitObjects]") {
            objects = true;
            timingpoints = false;
        } else  if (line == "[TimingPoints]") {
            timingpoints = true;
        }

    });

    return beatmap;
}

function hitObjectToString(object) {
    switch (object) {
        case 64:
            return "Left";
        case 192:
            return "Up";
        case 320:
            return "Down";
        case 448:
            return "Right";
    }
}

function stringToHitObject(str) {
    switch (str) {
        case "Left":
            return 64;
        case "Up":
            return 192;
        case "Down":
            return 320;
        case "Right":
            return 448;
    }
}

function stringToOrderValue(str) {
    switch (str) {
        case "Up":
            return 0;
        case "Right":
            return 1;
        case "Down":
            return 2;
        case "Left":
            return 3;
    }
}

function compNote(n1, n2) {
    return stringToOrderValue(n1) > stringToOrderValue(n2);
}

function completeIntralismObject(current, next) {
    if (current == null) {
        current = next;
    } else {
        var parts = current.split("-");
        parts.push(next);
        parts.sort(compNote);
        current = parts.join('-');
    }

    return current;
}

function maniaToIntralism(beatmap) {
    var offsetManiaToIntralism = 80; // Trial and error, in ms. Positive = notes are placed later than the song, Negative = notes are placed earlier.
    var speed = 25; // set to whataver
    var res = {
        "configVersion": 2,
        "name": beatmap.Artist + " - " + beatmap.Title,
        "info": beatmap.Creator + " - [ " + beatmap.Version + " ]",
        "levelResources": [],
        "tags": ["OneHand"],
        "handCount": 1,
        "moreInfoURL": "",
        "speed": speed,
        "lives": 50,
        "maxLives": 50,
        "musicFile": "music.ogg",
        "musicTime": 0,
        "iconFile": beatmap.Background,
        "environmentType": -1,
        "unlockConditions": [],
        "hidden": false,
        "checkpoints": [],
        "events": []
    };

    var events = {};
    each(beatmap.HitObjects, function (hitObject, k, o) {
        var key = hitObjectToString(hitObject.x);
        events[hitObject.time] = completeIntralismObject(events[hitObject.time], key);
    });

    var svs = {};
    var baseSpeed = speed;
    var base = 0;
    var first = true;
    each(beatmap.timingPoints, function (timingPoint, k, o) {
        if(timingPoint.val<0) {
            svs[timingPoint.time] = 14 / (100 / -timingPoint.val);
        }
    });

    each(svs, function (sv, t, o) {
        var time = (parseInt(t) + offsetManiaToIntralism) / 1000;
        
        
        res.events.push({
            time: time,
            data: [
                "SetPlayerDistance",
                sv
            ]
        });
    });

    var endTime = 0;
    each(events, function (keys, t, o) {
        var time = (parseInt(t) + offsetManiaToIntralism) / 1000;
        res.events.push({
            time: time,
            data: [
                "SpawnObj",
                "[" + keys + "],0"
            ]
        })

        if (time > endTime) endTime = time;
    });

    res.musicTime = endTime + 3;

    return res;
}
function intralismToMania(beatmap) {
    var offsetIntralismToMania = -80; // Trial and error, in ms. Positive = notes are placed later than the song, Negative = notes are placed earlier.
    var lines = [
        "osu file format v14",
        "",
        "[General]",
        "AudioFilename: audio.mp3",
        "AudioLeadIn: 0",
        "PreviewTime: -1",
        "Countdown: 0",
        "SampleSet: Normal",
        "StackLeniency: 0.7",
        "Mode: 3",
        "LetterboxInBreaks: 0",
        "SpecialStyle: 0",
        "WidescreenStoryboard: 0",
        "",
        "[Editor]",
        "DistanceSpacing: 1",
        "BeatDivisor: 4",
        "GridSize: 4",
        "TimelineZoom: 2.1",
        "",
        "[Metadata]",
        "Title:" + beatmap.Title,
        "TitleUnicode:" + beatmap.Title,
        "Artist:" + beatmap.Artist,
        "ArtistUnicode:" + beatmap.Artist,
        "Creator:" + beatmap.Creator,
        "Version:" + beatmap.Version,
        "Source:Intralism",
        "Tags:",
        "BeatmapID:0",
        "BeatmapSetID:-1",
        "",
        "[Difficulty]",
        "HPDrainRate:5",
        "CircleSize:4",
        "OverallDifficulty:10",
        "ApproachRate:5",
        "SliderMultiplier:1.4",
        "SliderTickRate:1",
        "",
        "[Events]",
        "//Background and Video events",
        "//Break Periods",
        "//Storyboard Layer 0 (Background)",
        "//Storyboard Layer 1 (Fail)",
        "//Storyboard Layer 2 (Pass)",
        "//Storyboard Layer 3 (Foreground)",
        "//Storyboard Sound Samples",
        "",
        "[TimingPoints]",
        "1,500,4,1,0,100,1,0",
        "",
        "[HitObjects]",
    ];

    each(beatmap.HitObjects, function (key, val, o) {
        lines.push(key.x + ",192," + (key.time + offsetIntralismToMania) + ",1,0,0:0:0:0:");
    });

    return lines;
}

function saveIntralismMap(dir, map) {
    fs.writeFileSync(dir + "/config.txt", JSON.stringify(map));
}

function saveOsuMap(dir, map, lines) {
    var contents = "";
    each(lines, function (key, val, o) {
        contents += key + "\n";
    });
    fs.writeFileSync(dir + "/" + map.Artist + " - " + map.Title + " (" + map.Creator + ") [" + map.Version + "].osu", contents);
}

function osuAudioToOgg(dir, map) {
    try {
        var path = map.path.split("/");
        path.pop();
        var folder = path.join("/");
        var audioFile = folder + "/" + map.AudioFilename;
        var parts = map.AudioFilename.split(".");

        new ffmpeg()
            .input(audioFile)
            .inputFormat(parts[parts.length - 1])
            .toFormat("ogg")
            .save(dir + "/music.ogg");

    } catch (e) {
        console.log(e.code);
        console.log(e.msg);
    }
}

function intralismAudioToMP3(dir, map) {
    try {
        var path = map.path.split("/");
        path.pop();
        var folder = path.join("/");
        var audioFile = folder + "/" + map.AudioFilename;
        var parts = map.AudioFilename.split(".");

        new ffmpeg()
            .input(audioFile)
            .inputFormat(parts[parts.length - 1])
            .toFormat("mp3")
            .save(dir + "/audio.mp3");

    } catch (e) {
        console.log(e.code);
        console.log(e.msg);
    }
}

function convertAssets(dir, map, beatmap, intralismMap) {
    if (beatmap.Background != null) {
        var path = map.path.split("/");
        path.pop();
        var folder = path.join("/");

        try {
            fs.copyFileSync(folder + "/" + beatmap.Background, dir + "/" + beatmap.Background);

            /*
            intralismMap.levelResources.push({
                "name": "1",
                "type": "Sprite",
                "path": beatmap.Background,
            });

            intralismMap.unshift({
                "time": 0.01,
                "data": [
                    "ShowSprite",
                    "1,1,True,10"
                ]
            });
            */
        } catch (e) { }
    }
}

function parseIntralismBeatmap(map) {
    var beatmap = {
        Version: map.Version.replace(/[\"\\\/]/g, ''),
        Artist: map.Artist.replace(/[\"\\\/]/g, ''),
        Title: map.Title.replace(/[\"\\\/]/g, ''),
        Creator: map.Creator.replace(/[\"\\\/]/g, ''),
        Background: null,
        HitObjects: [],
    };

    var file = JSON.parse(fs.readFileSync(map.path, { encoding: 'utf8' }).replace(/^\uFEFF/, ''));

    each(file.events, function (key, val, o) {
        if (key.data && key.data[0] == "SpawnObj") {
            var objects = key.data[1].split("]")[0];
            objects = objects.split("[")[1];

            each(objects.split("-"), function (keys, vals, obj) {
                var hitObject = {
                    x: stringToHitObject(keys),
                    time: Math.floor(key.time * 1000),
                }

                beatmap.HitObjects.push(hitObject);
            });
        }
    });

    return beatmap;
}

async function main() {
    checkConfig();

    var osuFolder = getOsuFolder();
    var intralismFolder = getIntralismFolder();
    var workshopFolder = getWorkshopFolder();
    var osuPath = osuFolder + "/Songs";

    var choice = prompt("Mania -> Intralism (1) or Intralism -> Mania (2) ? : ");
    var songs = [];

    if (choice == "1") {
        metadata = cache.osu.songs;

        fs.readdirSync(osuPath).forEach(file => {
            var f = fs.statSync(osuPath + "/" + file);
            if (f.isDirectory()) {
                songs.push(file);
            }
        });

        scanPhase(osuPath, songs);
        var length = Object.keys(metadata).length;
        console.log("Loaded " + length + " mapsets.\n");

        if (length <= 0) {
            process.exit();
        }

        var result = selectBeatmap();
        var beatmap = parseBeatmap(result.selected);

        var intralismMap = maniaToIntralism(beatmap);
        var outputDir = intralismFolder + "/Editor/" + beatmap.Artist + " - " + beatmap.Title + " - " + beatmap.Version + " - " + beatmap.Creator;

        try {
            outputDir = outputDir.replace(/[\.?]/g, "");
            fs.mkdirSync(outputDir);
        } catch { }
        osuAudioToOgg(outputDir, result.selected);
        convertAssets(outputDir, result.selected, beatmap, intralismMap);
        saveIntralismMap(outputDir, intralismMap);
    } else {
        metadata = cache.intralism.songs;

        fs.readdirSync(workshopFolder).forEach(file => {
            var f = fs.statSync(workshopFolder + "/" + file);
            if (f.isDirectory()) {
                songs.push(file);
            }
        });

        intralismScanPhase(workshopFolder, songs);
        var length = Object.keys(metadata).length;
        console.log("Loaded " + length + " mapsets.\n");

        if (length <= 0) {
            process.exit();
        }

        var result = selectBeatmap();
        var beatmap = parseIntralismBeatmap(result.selected);


        var osuMap = intralismToMania(beatmap);
        var outputDir = osuPath + "/intralism-convert-" + beatmap.Artist + " - " + beatmap.Title;

        try {
            fs.mkdirSync(outputDir);
        } catch { }

        intralismAudioToMP3(outputDir, result.selected);
        saveOsuMap(outputDir, beatmap, osuMap);

    }
}

function getFolderSprites(path, fps = 0, offset = 15) {

    var folderContents = walkSync(path);

    var resources = [];
    var sprites = [];

    each(folderContents, function (val, k, o) {
        if (val.includes("jpg")) {
            var id = val.split(".")[0];

            resources.push({
                "name": id,
                "type": "Sprite",
                "path": id + ".jpg"
            });
            sprites.push({
                "time": Math.round((offset / 1000 + id * (1 / fps)) * 1000) / 1000,
                "data": [
                    "ShowSprite",
                    id + ",0,True," + (1 / fps) + ",0,0"
                ]
            });
        }
    });

    return {
        levelResources: resources,
        events: sprites
    };
}

function execCriticalShellCommand(command, error = "Unspecified", options = {}) {
    if(shell.exec(command, options).code !== 0) {
        console.log("Error: "+error+".");
        process.exit();
    }
}

if (process.argv.length == 2) {
    main();
} else {

    switch (process.argv[2]) {
        case "getArcs":
            var path = process.argv[3];
            var events = [];

            var file = JSON.parse(fs.readFileSync(path + "/config.txt", { encoding: 'utf8' }).replace(/^\uFEFF/, ''));

            each(file.events, function (key, val, o) {
                if (key.data && key.data[0] == "SpawnObj") {
                    events.push(key);
                }
            });

            console.log(JSON.stringify(events));
            break;
        case "getSprites":
            var path = process.argv[3];
            var events = [];

            var file = JSON.parse(fs.readFileSync(path + "/config.txt", { encoding: 'utf8' }).replace(/^\uFEFF/, ''));

            each(file.events, function (key, val, o) {
                if (key.data && key.data[0] == "ShowSprite") {
                    events.push(key);
                }
            });

            console.log(JSON.stringify(events));
            break;
        case "getIntralismArcsFromMania":
            var path = process.argv[3];
            var events = [];

            var beatmap = parseBeatmap({ path: path }, false);

            beatmap.Title = "";
            beatmap.Version = "";
            beatmap.Creator = "";
            beatmap.Background = "";

            var iBeatmap = maniaToIntralism(beatmap);
            console.log(JSON.stringify(iBeatmap.events));
            break;
        case "replaceArcs":
            var path = process.argv[3];
            var events = [];

            var beatmap = parseBeatmap({ path: path }, false);

            beatmap.Title = "";
            beatmap.Version = "";
            beatmap.Creator = "";
            beatmap.Background = "";

            var iBeatmap = maniaToIntralism(beatmap);

            var out = process.argv[4];
            var file = JSON.parse(fs.readFileSync(out + "/config.txt", { encoding: 'utf8' }).replace(/^\uFEFF/, ''));

            each(file.events, function (key, val, o) {
                if (key.data && key.data[0] != "SpawnObj") {
                    iBeatmap.events.push(key);
                }
            });
            file.events = iBeatmap.events;
            fs.writeFileSync(out + "/config.txt", JSON.stringify(file), { encoding: 'utf8', flag: 'w' });
            break;
        case "getFolderSprites":
            var path = process.argv[3];
            var fps = 10;
            var offset = 0;

            if (process.argv.length >= 5) {
                fps = process.argv[4];
            }

            if (process.argv.length >= 6) {
                offset = process.argv[5];
            }

            getFolderSprites(path, fps, offset);
            break;
        case "addVideo":
            if (!shell.which('ffmpeg')) {
                console.log("Sorry, this script requires ffmpeg");
                process.exit();
            }
            var video = process.argv[3];
            var vparts = video.split(".");
            if (vparts[vparts.length - 1] != "mp4") {
                console.log("Use mp4 video.");
                process.exit();
            }
            var path = process.argv[4];
            var file;
            if (!fs.existsSync(path + "/config.txt")) {
                console.log("Can't find config.txt");
                process.exit();
            } else {
                try {
                    file = JSON.parse(fs.readFileSync(path + "/config.txt", { encoding: 'utf8' }).replace(/^\uFEFF/, ''));
                    console.log("Parsed the config file.");
                } catch {
                    console.log("Error: The provided map's config.txt file is malformed.");
                    process.exit();
                }
            }
            var fps = 10;
            var offset = 0;

            if (process.argv.length >= 6) {
                fps = process.argv[5];
            }

            if (process.argv.length >= 7) {
                offset = process.argv[6];
            }

            var tmp = process.cwd()+'\\tmp';
            
            if (!fs.existsSync(tmp)) {
                fs.mkdirSync(tmp);
            } else {
                shell.rm('-rf', tmp+"\\*");
            }
            shell.cd(tmp);

            console.log("Copying video...");
            execCriticalShellCommand("cp \""+video+"\" \""+tmp+"\"","Could not copy video file");
            var vslash = video.split("/");
            var vslash2 = vslash[vslash.length -1].split("\\");
            console.log("Cutting video...");
            execCriticalShellCommand("ffmpeg -i \""+tmp+"\\"+vslash2[vslash2.length-1]+"\" -r "+fps+" %d.jpg","Could not transform the video file", { silent: true});
            var sprites = getFolderSprites(tmp, fps, offset);

            console.log("Copying "+sprites.levelResources.length+" sprites...");
            execCriticalShellCommand("cp \""+tmp+"\\*.jpg\" \""+path+"\"","Could not copy the images");
            console.log("Cleaning...");
            shell.rm('-rf', tmp+"\\*");
            console.log("Saving "+sprites.events.length+" new events...");

            file.levelResources = file.levelResources.concat(sprites.levelResources);
            file.events = file.events.concat(sprites.events);

            fs.writeFileSync(path + "/config.txt", JSON.stringify(file), { encoding: 'utf8', flag: 'w' });
            console.log("Done.");
            break;
        case "checkOverlap":
            var path = process.argv[3];
            var events = [];

            var file = JSON.parse(fs.readFileSync(path + "/config.txt", { encoding: 'utf8' }).replace(/^\uFEFF/, ''));

            var last = {
                "Right": -1,
                "Left": -1,
                "Up": -1,
                "Down": -1,
            }
            each(file.events, function (key, val, o) {
                if (key.data && key.data[0] == "SpawnObj") {
                    each(key.data[1].split(",")[0].split("[")[1].split("]")[0].split("-"), function (col) {

                        if (last[col] > 0) {
                            var time = key.time;
                            var ec = time - last[col];

                            if (time - last[col] < 0.05) {
                                console.log("Warning! " + (ec * 1000) + "ms difference at " + time + " on column " + col + ".");
                            }
                        }
                        last[col] = key.time;
                    });
                }
            });
            break;
        case "satellize":
            var path = process.argv[3];
            var events = [];

            var file = JSON.parse(fs.readFileSync(path + "/config.txt", { encoding: 'utf8' }).replace(/^\uFEFF/, ''));

            file.events.push(
                {
                    "time": 0.001,
                    "data": [
                        "AddEnvironmentObject",
                        "0,sun_gen1"
                    ]
                });
            file.events.push(
                {
                    "time": 0.001,
                    "data": [
                        "AddEnvironmentObject",
                        "0,sun_gen2"
                    ]
                });
            file.events.push(
                {
                    "time": 0.002,
                    "data": [
                        "SetSunColors",
                        "sun_gen1,82fc5d,cafc5d"
                    ]
                });
            file.events.push(
                {
                    "time": 0.002,
                    "data": [
                        "SetSunColors",
                        "sun_gen2,82fc5d,cafc5d"
                    ]
                });
            var base_distance = 4;
            var resolution = 10;
            var byebye = 0;


            var last_time = -1;
            var last_x = 0;
            var last_y = 0;
            each(file.events, function (key, val, o) {

                if (key.data && key.data[0] == "SpawnObj") {
                    var distance_t = key.time - last_time;
                    var x = 0;
                    var y = 0;

                    if (Math.random() * 1000 > 500) {
                        base_distance += byebye;
                    } else {
                        base_distance -= byebye;
                    }

                    switch (key.data[1].split(",")[0].split("[")[1].split("]")[0]) {
                        case "Up":
                            y = base_distance;
                            break;
                        case "Down":
                            y = -base_distance;
                            break;
                        case "Right":
                            x = base_distance;
                            break;
                        case "Left":
                            x = -base_distance;
                            break;
                        case "Up-Right":
                            x = base_distance;
                            y = base_distance;
                            break;
                        case "Up-Left":
                            x = -base_distance;
                            y = base_distance;
                            break;
                        case "Up-Down":
                            x = base_distance * -Math.sign(last_x);
                            y = 0;
                            break;
                        case "Right-Left":
                            x = 0;
                            y = base_distance * -Math.sign(last_y);
                            break;
                        case "Right-Down":
                            x = base_distance;
                            y = -base_distance;
                            break;
                        case "Down-Left":
                            x = -base_distance;
                            y = -base_distance;
                            break;
                        case "Up-Right-Left":
                            x = 0;
                            y = base_distance;
                            break;
                        case "Up-Down-Left":
                            x = -base_distance;
                            y = 0;
                            break;
                        case "Up-Right-Down":
                            x = base_distance;
                            y = 0;
                            break;
                        case "Right-Down-Left":
                            x = 0;
                            y = -base_distance;
                            break;
                        case "Up-Right-Down-Left":
                            x = base_distance * -Math.sign(last_x);
                            y = base_distance * -Math.sign(last_y);
                            break;
                    }

                    if (last_time != -1) {
                        var delta_x = last_x - x;
                        var delta_y = last_y - y;

                        for (var i = 0; i < resolution; i++) {
                            var curr_x = x + ((delta_x / resolution) * i);
                            var curr_y = y + ((delta_y / resolution) * i);

                            var angle = Math.atan2(curr_y, curr_x);

                            file.events.push(
                                {
                                    "time": key.time - (distance_t - (distance_t / resolution) * i),
                                    "data": [
                                        "SetPosition",
                                        ("sun_gen1," + base_distance * Math.cos(angle) + "," + base_distance * Math.sin(angle) + ",0")
                                    ]
                                }
                            );

                            file.events.push(
                                {
                                    "time": key.time - (distance_t - (distance_t / resolution) * i),
                                    "data": [
                                        "SetPosition",
                                        ("sun_gen2," + (-base_distance * Math.cos(angle)) + "," + (-base_distance * Math.sin(angle)) + ",0")
                                    ]
                                }
                            );
                        }
                    } else {
                        file.events.push(
                            {
                                "time": key.time,
                                "data": [
                                    "SetPosition",
                                    ("sun_gen1," + x + "," + y + ",0")
                                ]
                            }
                        );
                        file.events.push(
                            {
                                "time": key.time,
                                "data": [
                                    "SetPosition",
                                    ("sun_gen2," + (-x) + "," + (-y) + ",0")
                                ]
                            }
                        );
                    }

                    last_x = x;
                    last_y = y;
                    last_time = key.time;
                }

            });

            fs.writeFileSync(path + "/config.txt", JSON.stringify(file), { encoding: 'utf8', flag: 'w' });
            console.log("Events: " + file.events.length);
            break;
    }
}
